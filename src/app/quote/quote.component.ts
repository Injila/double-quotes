import { Host, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Quote } from "./quote.model";

import { DataService } from "../data.service";
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/shared/services';

@Component({
  selector: 'quote-editor',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit, OnDestroy {
  
    text_title ="";
    imageLoader = document.getElementById('imageLoader');
    width: any;
    height: any;
    canvas = <HTMLCanvasElement>document.getElementById('imageCanvas');
    ctx: any;
    img = <HTMLImageElement>document.getElementById('canvasImage');
    quote : Quote = new Quote();
    message:string="";
    subscription: Subscription;
    username: string="";
    imageObject = [{
        image: 'https://i.picsum.photos/id/62/700/500.jpg?hmac=_0Tfn1s36mdMiunc7U-ima2vxVxLvhemXoxfiCBwDWs',
        thumbImage: 'https://i.picsum.photos/id/62/700/500.jpg?hmac=_0Tfn1s36mdMiunc7U-ima2vxVxLvhemXoxfiCBwDWs'
    }, {
        image: 'https://i.picsum.photos/id/168/700/500.jpg?hmac=Bgxkr_EMVrMp2MxohD8LlEwMooa8Oaoqtm-gw2xNAKo',
        thumbImage: 'https://i.picsum.photos/id/168/700/500.jpg?hmac=Bgxkr_EMVrMp2MxohD8LlEwMooa8Oaoqtm-gw2xNAKo'
    },{
        image: 'https://i.picsum.photos/id/737/700/500.jpg?hmac=RLoXgDtbqgnXbBD3ErkimlxQ7iS4aQyG1N8423zg3us',
        thumbImage: 'https://i.picsum.photos/id/737/700/500.jpg?hmac=RLoXgDtbqgnXbBD3ErkimlxQ7iS4aQyG1N8423zg3us'
    }, {
        image: 'https://i.picsum.photos/id/933/700/500.jpg?hmac=j-uuK3sIqBZKpHx15w6Aun_GMnXYpq8mlC0XFiBWmSM',
        thumbImage: 'https://i.picsum.photos/id/933/700/500.jpg?hmac=j-uuK3sIqBZKpHx15w6Aun_GMnXYpq8mlC0XFiBWmSM'
    }, {
        image: 'https://i.picsum.photos/id/358/700/500.jpg?hmac=W4GaIUg0kGT-rY4sMp_4NFzb-AlhfJy6BzQ9aotdBmY',
        thumbImage: 'https://i.picsum.photos/id/358/700/500.jpg?hmac=W4GaIUg0kGT-rY4sMp_4NFzb-AlhfJy6BzQ9aotdBmY'
    }, {
        image: 'https://i.picsum.photos/id/557/700/500.jpg?hmac=FBn64yMP0xdTRwZrUZi6dmJCAywnshgtk9OHJTN3hq4',
        thumbImage: 'https://i.picsum.photos/id/557/700/500.jpg?hmac=FBn64yMP0xdTRwZrUZi6dmJCAywnshgtk9OHJTN3hq4'
    }, {
        image: 'https://i.picsum.photos/id/559/700/500.jpg?hmac=xBpOikd4I6m3HrN6-rlFx-N50UNfQiYmFSiZYEOIAvE',
        thumbImage: 'https://i.picsum.photos/id/559/700/500.jpg?hmac=xBpOikd4I6m3HrN6-rlFx-N50UNfQiYmFSiZYEOIAvE'
    }, {
        image: 'https://i.picsum.photos/id/694/700/500.jpg?hmac=NOphlzx_enU6igFYAGD2l883KSufehFejkLmSzk09ow',
        thumbImage: 'https://i.picsum.photos/id/694/700/500.jpg?hmac=NOphlzx_enU6igFYAGD2l883KSufehFejkLmSzk09ow'
    },{
        image: 'https://i.picsum.photos/id/327/700/500.jpg?hmac=2h9vQcDjJ1r7Stv-cKmiDGsTtd-cZRK0fxGXrfUzJnA',
        thumbImage: 'https://i.picsum.photos/id/327/700/500.jpg?hmac=2h9vQcDjJ1r7Stv-cKmiDGsTtd-cZRK0fxGXrfUzJnA'
    }, {
        image: 'https://i.picsum.photos/id/195/700/500.jpg?hmac=v0_e2NxO-sra8Z6LkBSiBOXgD2oMgj24rryWbR4sAcU',
        thumbImage: 'https://i.picsum.photos/id/195/700/500.jpg?hmac=v0_e2NxO-sra8Z6LkBSiBOXgD2oMgj24rryWbR4sAcU'
    }, {
        image: 'https://i.picsum.photos/id/456/700/500.jpg?hmac=YyLaKandJ-cvfGEEPju9-dGZD1sTTp1CUflpj6zaROs',
        thumbImage: 'https://i.picsum.photos/id/456/700/500.jpg?hmac=YyLaKandJ-cvfGEEPju9-dGZD1sTTp1CUflpj6zaROs'
    }];

    constructor(private data: DataService, private route: ActivatedRoute, private authService: AuthService) {
        this.subscription = this.data.currentMessage.subscribe(message => this.quote.value = message);
        this.route.params.subscribe(params => {
            this.quote.value = params['quoteValue'];
          });
          this.authService.getUser().subscribe((user) => {
            this.username =  user?.fullname || "";
          })
    }

    ngOnInit() {
        this.canvas = <HTMLCanvasElement>document.getElementById('imageCanvas');
        if(this.imageLoader) {
            this.imageLoader.addEventListener('change', this.handleImage, false);
        }
        if(this.canvas) {
            this.ctx = this.canvas.getContext('2d');
            // this.ctx.canvas.width = window.innerWidth;
            // this.ctx.canvas.height = window.innerHeight;
        }
        this.img = <HTMLImageElement>document.getElementById('canvasImage');
        this.img.crossOrigin="anonymous";
        window.addEventListener('load', this.DrawPlaceholder);
        this.DrawPlaceholder();
        this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
    }

    selectImage($event: any) {
        console.log("The captured event: ", $event);
        this.img.src = this.imageObject[$event].image;
        if(document.getElementById('canvasImage') != null) {
            (<HTMLImageElement>document.getElementById('canvasImage')).src = this.imageObject[$event].image;
        }
        this.DrawPlaceholder();
    }

    DrawPlaceholder() {
        console.log("DrawPlaceholder");
        this.width = this.img.width;
        this.height = this.img.height;
        let self= this;
        if(this.img.src == "" || typeof this.img.src == 'undefined') {
            this.img.src = 'https://i.picsum.photos/id/405/700/500.jpg?hmac=SoAa70A4yJDxzmQ0vQhzQPRTMiSddcxMomtzfz03wt8';
        }
        this.img.onload = function() {
            self.DrawOverlay(self.img);
            self.DrawText();
            self.DynamicText(self.img)
        };
    }

    DrawOverlay(img: any) {
        console.log("DrawOverlay");
        this.ctx.drawImage(img,0,0);
        this.ctx.fillStyle = 'rgba(0, 0, 0, 0.3)';
        if(this.canvas) {
            this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        }
        
    }
    DrawText() {
        console.log("DrawText");
        this.ctx.fillStyle = "white";
        this.ctx.textBaseline = 'middle';
        this.ctx.font = "50px 'Montserrat'";
        this.ctx.fillText(this.quote.value, 50, 50);
        this.ctx.fillText("~" + this.username, 350, 400);
    }
    DynamicText(img: any) {
        console.log("DynamicText");
        let self=this;
        if(document != null && document.getElementById('name') != null ) {
            document!.getElementById('name')!.addEventListener('keyup', function() {
                let element = <HTMLInputElement>this;
                self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);
                self.DrawOverlay(img);
                self.DrawText(); 
                self.quote.value = element.value;
                self.ctx.fillText(self.quote.value, 50, 50);
                self.ctx.fillText("~" + self.username, 350, 400);
            });
        }
    }
    handleImage(e: any) {
        console.log("handleImage");
        var reader = new FileReader();
        var img = "";
        var src = "";
        let self=this;
        reader.onload = function(event) {
            self.img = new Image();
            self.img.onload = function() {
                // self.canvas.width = self.img.width;
                // self.canvas.height = self.img.height;
                self.ctx.drawImage(img,0,0);
            }
            if(event && event.target && event.target.result != null) {
                self.img.src = <string>event.target.result;
                src = <string>event.target.result;
            }
            self.canvas.classList.add("show");
            self.DrawOverlay(img);
            self.DrawText(); 
            self.DynamicText(img);   
        }

        reader.readAsDataURL(e.target.files[0]); 
    
    }
    convertToImage() {
        console.log("convertToImage");
        window.open(this.canvas.toDataURL('png'));
    }

    addQuote(event: any): void {
        console.log(event);
    }
    // document.getElementById('download').onclick = function download() {
    //         this.convertToImage();
    // }

    save(): void {

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
      }
}


