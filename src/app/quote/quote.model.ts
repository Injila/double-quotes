import { Visibility } from "../visibility.model";

export class Quote {
    visibility: Visibility = new Visibility();
    value: string = "";
}