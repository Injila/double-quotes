export class Visibility {
    publicVisibility: boolean = false;
    listeners: boolean = false;
    whispers: boolean = false;
}