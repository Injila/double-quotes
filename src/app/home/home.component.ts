import { Component, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Output, EventEmitter } from '@angular/core';
import { Quote } from "../quote/quote.model";
import { AbstractControl, FormControl, FormControlName, FormGroup } from '@angular/forms';
import { DataService } from "../data.service";
import { Subscription } from 'rxjs';

interface Visibility {
  value: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  visibilities: Visibility[] = [
    {value: 'Public'},
    {value: 'Only listeners'},
    {value: 'Whisper into the ears..'}
  ];

  message:string="";
  subscription: Subscription=new Subscription();
  @Output() newItemEvent = new EventEmitter<string>();

  selectedValue: any;
  newQuote: Quote = new Quote();
  form: FormGroup = new FormGroup({
    quoteValue:  new FormControl('')
  })
  //color test
  //homeGroup: FormGroup;

  constructor(private router: Router, private data: DataService) {
    // this.homeGroup = new FormGroup({
    //   quote: new FormControlName()
    // });
    //this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
  }

  ngOnInit() {
    this.selectedValue = this.visibilities[1];
    this.newQuote.value = "";
    this.subscription = this.data.currentMessage.subscribe(message => this.message = message);
  }

  quote(value: string): void {
    console.log('____Quoting________');
    this.router.navigate(['quote', value]);
  }

  inputChanged(event : any): void {
    console.log("Value changed: ", this.newQuote.value);
  }

  OnInput(event: any ) {
    debugger;
    if(this != null && this.form.get('quoteValue') != null) {
      let temp = this.form.get('quoteValue');
      if(temp != null) {
        this.data.changeMessage(temp.value);
        this.quote(temp.value);
      }
      else {
        this.quote("");
      }
      
    }
    else {
      this.quote("");
    }
    
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
