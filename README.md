# Double Quotes

This is a web application that will not only enable but encourage people to be philosophers and live a thoughtful life. The primary function of this would be to be able to add your quotes. A user can write something up and then select templates and fonts according to the quote or whichever they want. Once published, it will be visible to people and in their Activity if they allow it to, otherwise they can export it to Instagram profile as a post or they can just simply save the picture format.
One can receive appreciation on their posted quotes using a feature called claps. One is not allowed to repost a quote that is visible to them
The picture will contain their name as the author of the quote, a user can disable this feature, this will only remove their name from the picture and the quote will be all the time associated with some profile.

A user can follow other users, user will have the ability to have different collection of quotes and can save their own and other's quotes in their collection. Whoever has the visibility of your quote, can also save it in their collection but platform will maintain the ownership. Plagiarism would not be possible on this platform, One cannot write already existing quote of someone else as their own. On writing such a quote, the platform would publish the original author and quote (if it is publically visbile), otherwise it will just tell you that quote already exists.


